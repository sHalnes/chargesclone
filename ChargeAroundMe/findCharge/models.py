from django.db import models
from django.contrib.gis.db import models as gis_models
from django.contrib.gis import geos
#from geopy.geocoders.googlev3 import GoogleV3
#from geopy.geocoders.googlev3 import GeocoderQueryError

class ChargePoints(models.Model):
    charge_id = models.CharField(max_length=200, primary_key=True)
#    address = models.CharField(max_length=200)
    address = models.TextField()
    location = gis_models.PointField(u"longitude/latitude", geography=True, blank=True)#, null=True)
    #location = gis_models.PointField(u"longitude/latitude", null=True, blank=True,)

    gis = gis_models.GeoManager()
    object = models.Manager()

    def __str__(self):
        return self.charge_id

    # def save(self, **kwargs):
    #     if not self.location:
    #         address = u'%s' %(self.address)
    #         address = address.encode('utf-8')
    #         geocoder = GoogleV3()
    #         try:
    #             _, latlon = geocoder.geocode(address)
    #         except (GeocoderQueryError, ValueError):
    #             pass
    #         else:
    #             point = 'POINT(%s %s)' % (latlon[1], latlon[0])
    #             self.location = geos.fromstr(point)
    #     super(ChargePoints, self).save()


    def save(self, *args, **kwargs):
        if not self.charge_id or not self.location:
            return
        else:
            super(ChargePoints, self).save(*args, **kwargs)