import requests
import json
from .models import ChargePoints
# to test find_and_save(60.4757, 5.3172, 10)

def north_south_coords(latitude, longitude, distance):
    '''
    Takes coordinates of original position and distance ('radius') and count two coordinates of a rectangle.
    Formula: latitude +- 1/111 * distance
    :param latitude:
    :param longitude:
    :param distance: in km
    :return: northeast(latitude, longitude), southwest(longitude, latitude)
    '''
    latitude_NE = latitude + 1/111*distance
    latitude_SW = latitude - 1/111*distance

    return (latitude_NE, longitude), (latitude_SW, longitude)


# this function won't work right now since the API-key suddenly became not valid. Instead we will work on some
#saved data from file wholeJSON.txt
def find_and_save_old(latitude, longitude, distance):
    '''
    Find and save data about charging station in given radius using NOBIL API
    :param latitude:
    :param longitude:
    :param distance:
    :return:
    '''
    charge_id = []
    address = []
    location = []

    northeast, southwest = north_south_coords(latitude, longitude, distance)

    url = 'http://nobil.no/api/server/search.php'
    request = {'apikey': '9d74b4c676ecf89e8bc48da4a5628425',
               'apiversion': '3',
               'action': 'search',
               'type': 'rectangle',
               'northeast': northeast,
               'southwest': southwest,
               'extendings': '0'}

    response = requests.get(url, data=request)# here if needs we can change get by post (find out what's difference)
    print('we got response: ', response.status_code)
    if response.status_code == 200:
        # message that there is no charging stations takes less than 30 characters
        if len(response.text) > 30:
            print(response.text)
            try:
                data = json.loads(response.text)
                for i in range(len(data['chargerstations'])):
                    charge_id.append(data['chargerstations'][i]['csmd']['id'])
                    address_temp = [data['chargerstations'][i]['csmd']['Street'],
                                    data['chargerstations'][i]['csmd']['House_number']]
                    address.append(', '.join(address_temp))
                    location.append(data['chargerstations'][i]['csmd']['Position'])
            except(ValueError, KeyError, TypeError):
                print(ValueError, KeyError, TypeError)
                pass
            if charge_id and location:
                # here we have to call another script which will delite all points in db in certain rectangle. We'll do it to
                # renew information.
                for index in range(len(charge_id)):
                    lat, lon = location[index].strip('()').split(',')
                    charge_point = ChargePoints(charge_id=charge_id[index], address=address[index],
                                                location=GEOSGeometry("POINT(%s %s)" % (lon, lat)))
                    charge_point.save()


