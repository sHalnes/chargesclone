import json
import requests
from .models import ChargePoints
from django.contrib.gis.geos import GEOSGeometry
#from ChargeAroundMe.findCharge import models

# temporary function. Should be replaced with find_and_save(latitude, longitude, distance) as soon as I get a new api-key
def temp():

    charge_id = []
    address = []
    location = []
    try:
        with open('/Users/svetlana/PycharmProjects/ChargeAroundMe/ChargeAroundMe/findCharge/wholeJSON.txt', 'r') as file:
            content = json.loads(file.read())
            for i in range(len(content['chargerstations'])):
                charge_id.append(content['chargerstations'][i]['csmd']['id'])
                address_temp = [content['chargerstations'][i]['csmd']['Street'],
                                content['chargerstations'][i]['csmd']['House_number'],
                                content['chargerstations'][i]['csmd']['Description_of_location']]
                address.append(', '.join(address_temp))
                location.append(content['chargerstations'][i]['csmd']['Position'])

    except(FileNotFoundError):
        pass

    if charge_id and location:
        # here we have to call another script which will delite all points in db in certain rectangle. We'll do it to
        # renew information.
        for index in range(len(charge_id)):
            lat, lon = location[index].strip('()').split(',')
            charge_point = ChargePoints(charge_id=charge_id[index], address=address[index], location=GEOSGeometry("POINT(%s %s)" % (lon, lat)))
            charge_point.save()


def find_and_save(latitude, longitude, distance):
    '''
    Find and save data about charging station in given radius using NOBIL API
    :param latitude:
    :param longitude:
    :param distance:
    :return:
    '''
    charge_id = []
    address = []
    location = []

    url = 'http://nobil.no/api/server/search.php'
    request = {'apikey': '9d74b4c676ecf89e8bc48da4a5628425',
               'apiversion': '3',
               'action': 'search',
               'type': 'near',
               'lat': latitude,
               'long': longitude,
               'distance': str(distance*1000),
               'limit': '20'
               }

    response = requests.post(url, data=request)

    #response = requests.get(url, data=request)# here if needs we can change get by post (find out what's difference)
    if response.status_code == 200:
        # message that there is no charging stations takes less than 30 characters
        if len(response.text) > 30:
            try:
                data = json.loads(response.text)
                for i in range(len(data['chargerstations'])):
                    charge_id.append(data['chargerstations'][i]['csmd']['id'])
                    address_temp = [data['chargerstations'][i]['csmd']['Street'],
                                    data['chargerstations'][i]['csmd']['House_number'],
                                    data['chargerstations'][i]['csmd']['Description_of_location']]
                    address.append(', '.join(address_temp))
                    location.append(data['chargerstations'][i]['csmd']['Position'])
            except(ValueError, KeyError, TypeError):
                print(ValueError, KeyError, TypeError)
                pass
            if charge_id and location:
                # here we have to call another script which will delite all points in db in certain rectangle. We'll do it to
                # renew information.
                for index in range(len(charge_id)):
                    lat, lon = location[index].strip('()').split(',')
                    charge_point = ChargePoints(charge_id=charge_id[index], address=address[index],
                                                location=GEOSGeometry("POINT(%s %s)" % (lon, lat)))
                    charge_point.save()

