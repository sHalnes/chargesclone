from django import forms


class RadiusForm(forms.Form):
    searching_radius = forms.CharField(
        label='Searching radius is ',
        widget=forms.TextInput(
            attrs={'placeholder': 'type in radius in km'}
        ))