import json
import requests
from django.contrib.gis.measure import D
from django.shortcuts import render
from django.contrib.gis import geos
#from geopy.geocoders.googlev3 import GoogleV3
#from geopy.geocoders.googlev3 import GeocoderQueryError
from django.shortcuts import render_to_response
from django.template import RequestContext
#from ChargeAroundMe.findCharge.models import ChargePoints
#from ChargeAroundMe.findCharge.forms import AddressForm
from .models import ChargePoints
from .forms import RadiusForm
from .find_charging_stations import *
from django.contrib.gis import measure



# def geocode_address(address):
#     address = address.encode('utf-8')
#     geocoder = GoogleV3()
#     try:
# #        latlon = geocoder.geocode(address)
#         _, latlon = geocoder.geocode(address)
#     except (GeocoderQueryError, ValueError):
#         print("error")
#         return None
#     else:
#         return latlon


# Need to change later. The best way is to get location from google API in javascript, I think. But right now let it be so
# while testing and debugging

def get_user_position():
    request = requests.get("http://ip-api.com/json")
    if request.status_code == 200:
        try:
            data = json.loads(request.text)
            return (data['lat'], data['lon'])
        except (ValueError, KeyError, TypeError):
            print("JSON format error.")
            #return (0.0, 0.0)
    #else:
        #return (0.0, 0.0)


def get_points(latitude, longitude, radius=10):
    current_point = geos.fromstr("POINT(%s %s)" % (longitude, latitude))
    distance_from_point = {'km': radius}
    charging_points = ChargePoints.gis.filter(location__distance_lte=(current_point, measure.D(**distance_from_point)))
    charging_points = charging_points.distance(current_point).order_by('distance')
    return charging_points.distance(current_point)


def index(request):
    charging_points = []
    # default lat lon
    latitude, longitude = (0, 0)#(60.388758, 5.328084)
    if request.method == 'POST':
        form = RadiusForm(request.POST)
        if form.is_valid():
            radius = form.cleaned_data['searching_radius']
            #location = geocode_address(address)
            location = get_user_position()
            if radius:
                try:
                    radius = int(radius)
                    latitude, longitude = location
                    # here we have to call a function wich will delite everything from db

                    # Sending the coordinates to another script which will find charging points in current location
                    # and save to db
                    find_and_save(latitude, longitude, radius)
                    #temp()

                    charging_points = get_points(latitude, longitude, radius)
                except(ValueError):
                    latitude, longitude = location
                    charging_points = get_points(latitude, longitude)
                    #pass
                #return HttpResponseRedirect('')
    else:
        form = RadiusForm()
    return render(request, 'index.html', {'form':form, 'charging_points': charging_points, 'latitude': latitude, 'longitude': longitude})#, RequestContext(request))  # probably have to change to index here


